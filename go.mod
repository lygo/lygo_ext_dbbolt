module bitbucket.org/lygo/lygo_ext_dbbolt

go 1.16

require (
	bitbucket.org/lygo/lygo_commons v0.1.120
	bitbucket.org/lygo/lygo_events v0.1.10
	github.com/google/uuid v1.3.0 // indirect
	go.etcd.io/bbolt v1.3.6
	golang.org/x/sys v0.0.0-20210909193231-528a39cd75f3 // indirect
)
